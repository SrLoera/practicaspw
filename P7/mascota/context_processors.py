from .models import Mascota
import time 

def messageVersion(request):
	context = {"msgVersion":"MacotasApp v0.0.1",}
	return context

def countMascotas(request):
	context = {"countMas": Mascota.objects.count(),}
	return context

def getDate(request):
	context = {"date": time.strftime("%c")}
	return context