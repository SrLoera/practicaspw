from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q


# Create your views here.

from .models import Mascota
from .forms import MascotaForm

##Modulos necesarios para APIS 
from .serializers import MascotaSerialize
from rest_framework import generics

#Vistas de las APIS
class MascotaAPICreate(generics.CreateAPIView):
	serializer_class = MascotaSerialize

	def perform_create(self , serializer):
		serializer.save()


class MascotaAPIList(generics.ListAPIView):
	serializer_class = MascotaSerialize

	def get_queryset(self , *args , **kwargs):
		return Mascota.objects.all()


# CREATE
def create(request):
	form = MascotaForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "mascota/create.html", context)

# RETRIEVE
def list(request):
	queryset = Mascota.objects.all()
	context = {
		"mascotas": queryset
	}
	return render(request, "mascota/list.html", context)

def detail(request, id):
 	queryset = Mascota.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "mascota/detail.html", context)


# UPDATE
def update(request, id):
	mascota = Mascota.objects.get(id=id)
	if  request.method == "GET":
		form = MascotaForm(instance=mascota )
	else:
		form = MascotaForm(request.POST, instance=mascota )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "mascota/update.html", context)

# DELETE
def delete(request, id):
	mascota = Mascota.objects.get(id=id)
	if request.method == "POST":
		mascota.delete()
		return redirect("list")
	context = {
		"object": mascota
	}
	return render(request, "mascota/delete.html", context)

#Vistas genericas 
class List(generic.ListView):
	template_name = "mascota/list2.html"
	queryset = Mascota.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Mascota.objects.all()
		query = self.request.GET.get('q', None)
		if(query):
			qs = qs.filter( Q( nombre__icontains = query ) )

		return qs

class Detail(generic.DetailView):
	template_name = "mascota/detail2.html"
	model = Mascota

class Create(generic.CreateView):
	template_name = "mascota/create2.html"
	model = Mascota
	fields = [
			"nombre",
			"edad",
			"especie",
			"sexo",
			"estado",
		]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "mascota/update2.html"
	model = Mascota
	fields = [
			"nombre",
			"edad",
			"especie",
			"sexo",
			"estado",
		]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "mascota/delete2.html"
	model = Mascota
	success_url = reverse_lazy("list")
