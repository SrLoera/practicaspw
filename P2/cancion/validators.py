from django.core.exceptions import ValidationError
import re

def validation_duration(value):
	duration = value 
	re_duration = "^[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$"
	if(re.match(re_duration , duration) == None):
		raise ValidationError("It is not valid")
	return value

def validation_genero(value):
	genero = value
	if(genero.upper() not in ["RAP","ROCK","POP","HIPHOP", "ELECTRONIC" , "BANDA", "JAZ"]):
		raise ValidationError("It is not valid")
	return value