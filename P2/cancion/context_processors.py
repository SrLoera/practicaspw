from .models import Cancion
import time 

def messageVersion(request):
	context = {"msgVersion":"AutosApp v0.0.1",}
	return context

def countCanciones(request):
	context = {"countCan": Cancion.objects.count(),}
	return context

def getDate(self):
	context = {"date": time.strftime("%c")}
	return context