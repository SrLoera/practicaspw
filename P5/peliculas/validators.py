from django.core.exceptions import ValidationError

import re 

regular_duration = "^[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$"

def validation_duration(value):
	duration = value
	if(re.match(regular_duration,duration) == None):
		raise ValidationError("Please, insert a correct Duration")
	return value

