from .models import Pelicula
import time 

def messageVersion(request):
	context = {"msgVersion":"PeliculasApp v0.0.1",}
	return context

def countPeliculas(request):
	context = {"countPel": Pelicula.objects.count(),}
	return context

def getDate(request):
	context = {"date": time.strftime("%c")}
	return context