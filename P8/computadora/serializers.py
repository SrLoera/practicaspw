from rest_framework import serializers

from .models import Computadora 


class ComputadoraSerialize(serializers.ModelSerializer):
	class Meta:
		model = Computadora
		fields = [
			"modelo",
			"marca",
			"procesador",
			"ram",
			"disco",
			"estado",
		]