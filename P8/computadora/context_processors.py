from .models import Computadora
import time 

def messageVersion(request):
	context = {"msgVersion":"ComputadorasApp v0.0.1",}
	return context

def countComputadoras(request):
	context = {"countComp": Computadora.objects.count(),}
	return context

def getDate(request):
	context = {"date": time.strftime("%c")}
	return context