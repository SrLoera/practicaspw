from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q


# Create your views here.

from .models import Pokemon
from .forms import PokemonForm

##Modulos necesarios para APIS 
from .serializers import PokemonSerialize
from rest_framework import generics

#Vistas de las APIS
class PokemonAPICreate(generics.CreateAPIView):
	serializer_class = PokemonSerialize

	def perform_create(self , serializer):
		serializer.save()


class PokemonAPIList(generics.ListAPIView):
	serializer_class = PokemonSerialize

	def get_queryset(self , *args , **kwargs):
		return Pokemon.objects.all()

# CREATE
def create(request):
	form = PokemonForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "pokemon/create.html", context)
# RETRIEVE
def list(request):
	queryset = Pokemon.objects.all()
	context = {
		"pokemons": queryset
	}
	return render(request, "pokemon/list.html", context)

def detail(request, id):
 	queryset = Pokemon.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "pokemon/detail.html", context)


# UPDATE
def update(request, id):
	pokemon = Pokemon.objects.get(id=id)
	if  request.method == "GET":
		form = PokemonForm(instance=pokemon )
	else:
		form = PokemonForm(request.POST, instance=pokemon )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "pokemon/update.html", context)

# DELETE

def delete(request, id):
	pokemon = Pokemon.objects.get(id=id)
	if request.method == "POST":
		pokemon.delete()
		return redirect("list")
	context = {
		"object": pokemon
	}
	return render(request, "pokemon/delete.html", context)

#Vistas genericas

class List(generic.ListView):
	template_name = "pokemon/list2.html"
	queryset = Pokemon.objects.filter()


	def get_queryset(self, *args, **kwargs):
		qs = Pokemon.objects.all()
		query = self.request.GET.get('q', None)
		if(query):
			qs = qs.filter( Q( nombre__icontains = query ) )

		return qs

class Detail(generic.DetailView):
	template_name = "pokemon/detail2.html"
	model = Pokemon

class Create(generic.CreateView):
	template_name = "pokemon/create2.html"
	model = Pokemon
	fields = [
			"nombre",
			"tipo",
			"nivel",
			"experiencia",
			"estado",
		]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "pokemon/update2.html"
	model = Pokemon
	fields = [
			"nombre",
			"tipo",
			"nivel",
			"experiencia",
			"estado",
		]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "pokemon/delete2.html"
	model = Pokemon
	success_url = reverse_lazy("list")