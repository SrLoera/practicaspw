from .models import Pokemon
import time 

def messageVersion(request):
	context = {"msgVersion":"PokemonsApp v0.0.1",}
	return context

def countPokemons(request):
	context = {"countPok": Pokemon.objects.count(),}
	return context

def getDate(request):
	context = {"date": time.strftime("%c")}
	return context