from django.core.exceptions import ValidationError

def validation_alimento(value):
	alimento = value
	if(alimento not in ["Herbivoro","Carnivoro","Omnivoro","Insectivoro"]):
		raise ValidationError("It is not valid")
	return value

def validation_ecosistema(value):
	ecositema = value
	if(ecositema not in ["Mar salado","Mar dulce","Sabana","Bosque", "Selva" , "Nevada", "Desierto", "Montaña"]):
		raise ValidationError("It is not valid")
	return value
