from .models import Celular
import time 

def messageVersion(request):
	context = {"msgVersion":"CelularesApp v0.0.1",}
	return context

def countCelulares(request):
	context = {"countCel": Celular.objects.count(),}
	return context

def getDate(request):
	context = {"date": time.strftime("%c")}
	return context