from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q


# Create your views here.

from .models import Celular
from .forms import CelularForm

##Modulos necesarios para APIS 
from .serializers import CelularSerialize
from rest_framework import generics

#Vistas de las APIS
class CelularAPICreate(generics.CreateAPIView):
	serializer_class = CelularSerialize

	def perform_create(self , serializer):
		serializer.save()


class CelularAPIList(generics.ListAPIView):
	serializer_class = CelularSerialize

	def get_queryset(self , *args , **kwargs):
		return Celular.objects.all()

# CREATE
def create(request):
	form = CelularForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "celular/create.html", context)

# RETRIEVE
def list(request):
	queryset = Celular.objects.all()
	context = {
		"celulars": queryset
	}
	return render(request, "celular/list.html", context)

def detail(request, id):
 	queryset = Celular.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "celular/detail.html", context)


# UPDATE
def update(request, id):
	celular = Celular.objects.get(id=id)
	if  request.method == "GET":
		form = CelularForm(instance=celular )
	else:
		form = CelularForm(request.POST, instance=celular )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "celular/update.html", context)

# DELETE
def delete(request, id):
	celular = Celular.objects.get(id=id)
	if request.method == "POST":
		celular.delete()
		return redirect("list")
	context = {
		"object": celular
	}
	return render(request, "celular/delete.html", context)

#vistas genericas 

class List(generic.ListView):
	template_name = "celular/list2.html"
	queryset = Celular.objects.filter()


	def get_queryset(self, *args, **kwargs):
		qs = Celular.objects.all()
		query = self.request.GET.get('q', None )
		if(query):
			qs = qs.filter(Q( modelo__icontains = query ))

		return qs

class Detail(generic.DetailView):
	template_name = "celular/detail2.html"
	model = Celular

class Create(generic.CreateView):
	template_name = "celular/create2.html"
	model = Celular
	fields = [
			"modelo",
			"marca",
			"año",
			"so",
			"camaras",
			"compañias",
		]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "celular/update2.html"
	model = Celular
	fields = [
			"modelo",
			"marca",
			"año",
			"so",
			"camaras",
			"compañias",
		]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "celular/delete2.html"
	model = Celular
	success_url = reverse_lazy("list")


	