#Validators 
from django.core.exceptions import ValidationError
import re 

regular_email = "^[a-zA-Z0-9_]+@([a-zA-Z0-9]+.[a-zA-Z0-9]+)+$"

def validation_semester(value):
	semestre = value
	if(semestre > 12):
		raise ValidationError("The max semester is 12")
	return value 

def validation_email(value):
	email = value
	if(re.match(regular_email,email) == None):
		raise ValidationError("Please, insert a correct Email")
	return value
	
def validation_age(value):
	age = value
	if(age < 17):
		raise ValidationError("Too Young")
	return value