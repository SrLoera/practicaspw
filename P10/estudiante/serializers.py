from rest_framework import serializers

from .models import Estudiante 


class EstudianteSerialize(serializers.ModelSerializer):
	class Meta:
		model = Estudiante 
		fields = [
			"nombre",
			"apellido",
			"edad",
			"correo",
			"carrera",
			"semestre",
			"status"
		]

