from .models import Estudiante
import time 

def messageVersion(request):
	context = {"msgVersion":"EstudiantesApp v0.0.1",}
	return context

def countEstudiantes(request):
	context = {"countEst": Estudiante.objects.count(),}
	return context

def getDate(request):
	context = {"date": time.strftime("%c")}
	return context