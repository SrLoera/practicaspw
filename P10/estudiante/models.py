from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from estudiante import signals
from estudiante import validators

#Manejadores
class Estudiante_Queryset(models.QuerySet):
	def estudiante_inactivo(self):
		return self.filter(status = False)
	def estudiante_prorroga(self):
		return self.fiter(semestre = 12)
	def estudiante_nano(self):
		return self.filter(carrera = "Ing. Nanotecnologia")
	def estudiante_nano_prorroga(self):
		return self.filter(carrera = "Ing. Nanotecnologia" , semestre = 12)

# Create your models here.
class Estudiante(models.Model):
	nombre = models.CharField(max_length = 30)
	apellido = models.CharField(max_length = 30)
	edad = models.IntegerField(validators = [validators.validation_age])
	correo = models.CharField(max_length = 50,validators = [validators.validation_email])
	carrera = models.CharField(max_length = 30)
	semestre = models.IntegerField(validators = [validators.validation_semester])
	status = models.BooleanField()
	objects = Estudiante_Queryset.as_manager()
	
	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_estudiante_receiver , sender = Estudiante)
pre_save.connect(signals.pre_save_estudiante_receiver , sender = Estudiante)
pre_delete.connect(signals.pre_delete_estudiante_receiver , sender = Estudiante)
post_delete.connect(signals.post_delete_estudiante_receiver  , sender = Estudiante)







