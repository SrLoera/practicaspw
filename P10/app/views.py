from django.shortcuts import render

from .forms import LoginForm , Users_Form
from django.contrib.auth import authenticate, login 

from django.views import generic
from django.urls import reverse_lazy

# Create your views here.

class SignUp(generic.FormView):
	template_name = "app/signup.html"
	form_class = Users_Form
	success_url = reverse_lazy('login')


	def form_valid(self , form):
		user = form.save()
		return super(SignUp, self).form_valid(form) 


def index(request):
	message = "Not login"
	form = LoginForm(request.POST or None )
	if(request.method == "POST"):
		if(form.is_valid()):
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(username = username , password = password)
			if user is not None:
				if user.is_active:
					login(request , user )
					message = 'user logged'
				else:
					message = 'user is not active'
			else:
				message = 'user does not exist'

	context = {
		'form': form,
		'message': message,
	}
	return render(request, "app/index.html", context)

def about(request):
	context = {
		"name": "About",
		"message": "Us",
	}
	return render(request, "app/about.html", context)


def lista(request):
	context = {
			"listaDatos": [1, 5, 8, 3, 9]
		}
	return render(request, "app/list.html", context)


def template1(request):
	return render(request , 'app/template1.html' , {})

def template2(request):
	return render(request , 'app/template2.html' , {})

def template3(request):
	return render(request , 'app/template3.html' , {})

def template4(request):
	return render(request , 'app/template4.html' , {})

def template5(request):
	return render(request , 'app/template5.html' , {})

def template6(request):
	return render(request , 'app/template6.html' , {})

def template7(request):
	return render(request , 'app/template7.html' , {})

def template8(request):
	return render(request , 'app/template8.html' , {})

def template9(request):
	return render(request , 'app/template9.html' , {})

def template10(request):
	return render(request , 'app/template10.html' , {})




