from django.core.exceptions import ValidationError

def validation_pages(value):
	num_pages = value
	if(num_pages <= 0):
		raise ValidationError("Numeber of pages is not negative")
	return value

