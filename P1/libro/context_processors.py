from .models import Libro
import time 

def messageVersion(request):
	context = {"msgVersion":"LibrosApp v0.0.1",}
	return context

def countLibros(request):
	context = {"countLib": Libro.objects.count(),}
	return context

def getDate(request):
	context = {"date": time.strftime("%c")}
	return context