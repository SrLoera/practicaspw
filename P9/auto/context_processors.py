from .models import Auto
import time 

def messageVersion(request):
	context = {"msgVersion":"AutosApp v0.0.1",}
	return context

def countAutos(request):
	context = {"countAutos": Auto.objects.count(),}
	return context

def getDate(request):
	context = {"date": time.strftime("%c")}
	return context