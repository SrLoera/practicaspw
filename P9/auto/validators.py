from django.core.exceptions import ValidationError

import re 

regular_matricula = "^[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]$"

def validation_matricula(value):
	matricula = value
	if(re.match(regular_matricula,matricula) == None):
		raise ValidationError("Please, insert a correct Matricula")
	return value

